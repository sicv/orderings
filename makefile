#####################
# VARIABLES
#####################
# The variables of a makefile determine the precise names of files TO BE GENERATED
# the make file is essentially driven by these variables

# Find any images in the originals folder to use as originals (NB basenames must be unique)
originals = $(shell find originals/ -iname "*.jpg")
# basenames = $(basename $(notdir $(originals)))
basenames = $(notdir $(originals))

images1920 = $(addprefix image1920x/, $(addsuffix .png, $(basenames)))
images1920_json = $(images1920:%.png=%.json)

images640 = $(addprefix image640x/, $(addsuffix .png, $(basenames)))

channel_red = $(addprefix channel_red/, $(addsuffix .png, $(basenames)))
channel_red_json = $(channel_red:%.png=%.json)

channel_green = $(addprefix channel_green/, $(addsuffix .png, $(basenames)))
channel_green_json = $(channel_green:%.png=%.json)

channel_blue = $(addprefix channel_blue/, $(addsuffix .png, $(basenames)))
channel_blue_json = $(channel_blue:%.png=%.json)

texture_files=$(addprefix texture/, $(addsuffix .png, $(basenames)))
texture_files_json = $(texture_files:%.png=%.json)

sift_images=$(addprefix sift/, $(addsuffix .png, $(basenames)))
sift_json = $(sift_images:%.png=%.json)

lexicality_files=$(addprefix lexicality/, $(addsuffix .png, $(basenames)))
lexicality_files_json = $(lexicality_files:%.png=%.json)

contours_files=$(addprefix contours/, $(addsuffix .png, $(basenames)))
contours_files_json = $(contours_files:%.png=%.json)

gradient_images=$(addprefix gradient/, $(addsuffix .png, $(basenames)))
gradient_json = $(gradient_images:%.png=%.json)

### ADD new filter variables above and to the lists below...

derivs = $(images1920) $(images640) $(channel_red) $(channel_green) $(channel_blue) $(texture_files) $(lexicality_files) $(contours_files) $(sift_images) $(gradient_images)
json = $(images1920_json) $(channel_red_json) $(channel_green_json) $(channel_blue_json) $(texture_files_json) $(lexicality_files_json) $(contours_files_json) $(sift_json) $(gradient_json)


#####################
thumbs = $(derivs:%.png=%.thumb.png)
icons = $(derivs:%.png=%.icon.png)


############################
# RULES & RECIPES
############################
# Generic Rules & Recipes

# default rule
all: index.json $(derivs) $(thumbs) $(icons)

image1920x/index.json: $(images1920_json)
	python appendmeta.py $^ > $@

channel_red/index.json: $(channel_red_json)
	python appendmeta.py $^ > $@

channel_green/index.json: $(channel_green_json)
	python appendmeta.py $^ > $@

channel_blue/index.json: $(channel_blue_json)
	python appendmeta.py $^ > $@

texture/index.json: $(texture_files_json)
	python appendmeta.py $^ > $@

sift/index.json: $(sift_json)
	python appendmeta.py $^ > $@

lexicality/index.json: $(lexicality_files_json)
	python appendmeta.py $^ > $@

contours/index.json: $(contours_files_json)
	python appendmeta.py $^ > $@

gradient/index.json: $(gradient_json)
	python appendmeta.py $^ > $@

# Use imagemagick to make thumbs & icons
# Important for these rules to be first
# otherwise it attempts to make thumbs by applying filters to preview thumbs & icons :(
%.thumb.png: %.png
	convert -resize 320x $< $@

%.icon.png: %.png
	convert -resize 32x $< $@

# Use exiftool to extract json from an image
%.json: %.png
	exiftool -json $< > $@

%.html: %.md 
	pandoc --from markdown --to html --standalone -o $@ $<

# compilemeta accumulates an index of all the json files
# index.json: $(json)
index.json: image1920x/index.json channel_red/index.json channel_green/index.json channel_blue/index.json texture/index.json lexicality/index.json contours/index.json sift/index.json gradient/index.json
	python compilemeta.py $^ > $@

# Use make print-NAME to debug variables
# as in: make print-originals
print-%:
	@echo '$*=$($*)'

# Use make clean to force a regeneration of everything
clean:
	rm -rf images640x
	rm -rf images1920x
	rm -rf channel_red
	rm -rf channel_green
	rm -rf channel_blue
	rm -rf texture
	rm -rf sift
	rm -rf lexicality
	rm -rf contours
	rm index.json


# FILTER RULES & RECIPES
# The Recipes of a makefile (in this case implicit rules)
# Tell make how to produce a given filename (from the variables)
# These are "implicit rules" -- as they use the % wildcard to define a pattern transformation


image640x/%.png: originals/%
	mkdir -p image640x
	convert -resize 640x $< $@
	exiftool -MakerNote=image640x -OriginalRawFileName=$* $@ -overwrite_original

image1920x/%.png: originals/%
	mkdir -p image1920x
	convert -resize 1920x $< $@
	exiftool -MakerNote=preview -OriginalRawFileName=$* $@ -overwrite_original

channel_red/%.png: image1920x/%.png
	# Ensure channel_red folder exists (-p makes it not complain if it exists)
	mkdir -p channel_red
	# Actually produce the new image
	# Here TWO KEY MAKE variables:
	# $< : INPUT FILE NAME (right hand side of the rule)
	# $@ : OUTPUT FILE NAME (left hand side of the rule)
	python filters/channel.py --channel red --threshold 48 $< --output $@
	# Here a second script is used to extract the brightness value to a file
	python filters/imagevalue.py $@ > annotate.tmp
	mogrify -transparent black $@
	# And then exiftool is used to annotate the image ($@)
	# MakerNote is set to the name of the filter
	# UserComment should be the sortable value
	# (in this case <= means read the value from a file)
	exiftool -MakerNote=channel_red "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	# cleanup tmp file
	rm annotate.tmp

channel_green/%.png: image1920x/%.png
	mkdir -p channel_green
	python filters/channel.py --channel green --threshold 16 $< --output $@
	python filters/imagevalue.py $@ > annotate.tmp
	mogrify  -transparent black $@
	exiftool -MakerNote=channel_green "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm annotate.tmp

channel_blue/%.png: image1920x/%.png
	mkdir -p channel_blue
	python filters/channel.py --channel blue --threshold 16 $< --output $@
	python filters/imagevalue.py $@ > annotate.tmp
	mogrify -transparent black $@
	exiftool -MakerNote=channel_blue "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm annotate.tmp

texture/%.png: originals/%
	mkdir -p texture
	bin/texture $< $@.jpg > annotate.tmp || convert -size 100x100 canvas:black $@.jpg; echo 0 > annotate.tmp
	convert $@.jpg -transparent black -fuzz 5% $@
	exiftool -MakerNote=texture "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm $@.jpg
	rm annotate.tmp

sift/%.png: image640x/%.png
	mkdir -p sift
	python filters/sift.py $< --output sift/$*.sift --draw $@.svg --count > annotate.tmp
	convert -background transparent $@.svg $@
	mogrify -trim $@
	exiftool -MakerNote=sift "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm annotate.tmp

lexicality/%.png: originals/%
	mkdir -p lexicality
	bin/lexicality $< $@.jpg > annotate.tmp
	convert $@.jpg -transparent black -fuzz 5% $@
	exiftool -MakerNote=lexicality "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm $@.jpg
	rm annotate.tmp

contours/%.png: originals/%
	mkdir -p contours
	bin/contours $< $@.jpg 2> annotate.tmp > contours/$*.svg
	cat annotate.tmp | python filters/suppress_error_lines.py > annotate2.tmp
	convert $@.jpg -transparent black $@
	exiftool -MakerNote=contours "-UserComment<=annotate2.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm $@.jpg
	rm annotate.tmp annotate2.tmp

gradient/%.png: originals/%
	mkdir -p gradient
	filters/imagegradient $< --output $@ --svg --width 640 > annotate.tmp
	exiftool -MakerNote=gradient "-UserComment<=annotate.tmp" -OriginalRawFileName=$* $@ -overwrite_original
	rm annotate.tmp

### ADD New Filter Rules + Recipes here. NB: Makefiles REQUIRE TABS!!!!

scan:
	$(eval f=originals/zzscan_$(shell date -u +%Y-%m-%dT%H-%M-%SZ).pnm)
	scanimage --mode Color --resolution 300 > $(f) 
	convert $(f) $(f).jpg

sound:
	aplay /usr/share/sounds/purple/receive.wav