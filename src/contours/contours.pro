#-------------------------------------------------
#
# Project created by QtCreator 2012-05-26T20:16:46
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = contours
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

CONFIG +=link_pkgconfig
PKGCONFIG += opencv
