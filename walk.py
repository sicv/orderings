import json, os
from PIL import Image 
from argparse import ArgumentParser
from cStringIO import StringIO
from math import floor, ceil
from sh import inkscape
from random import choice, shuffle, randint


p = ArgumentParser("")
p.add_argument("--width", type=int, default=640)
p.add_argument("--height", type=int, default=480)
p.add_argument("--background", default="black")
p.add_argument("--output", default="output.mjpeg")
p.add_argument("--limit", type=int, default=5)
p.add_argument("--hold", type=int, default=10)
args = p.parse_args()


with open ("index.json") as f:
	data = json.load(f)
	# print json.dumps(data, indent=2	)

names = data.keys()
names.sort()
orderings = data[names[0]].keys()
orderings.sort()

# Created ordered lists per ordering
ordered_images = {}
for o in orderings:
	items = [x for x in data.values() if o in x and 'UserComment' in x[o]]
	items.sort(key=lambda d: d[o]['UserComment'], reverse=True)
	ordered_images[o] = items

	# print "Ordering {0}".format(o)
	# for i in items[:10]:
	# 	print i[o]
	# print "----"
	# print

def fixname (n):
	if n.startswith("sift/"):
		n = n + ".svg"
	elif n.startswith("contours/") or n.startswith("gradient/"):
		n = n[:-4]+".svg"
	return n

def usepath (ipath):
	ipath = fixname(ipath)
	if ipath.endswith(".svg"):
		inkscape("-w", str(args.width), "--export-png=tmp.png", ipath)
		ipath = "tmp.png"
	return ipath

def draw_layer(base, path):
    W, H = base.size
    im = Image.open(path)
    if im.mode != "RGBA":
    	im = im.convert("RGBA")
    im.thumbnail((W,H))
    w, h = im.size
    ix = (W-w)/2
    iy = (H-h)/2
    # bim = Image.new("RGB", (W,H), color=args.background)
    base.paste(im, (ix, iy), mask=im)

def nonzeroorderings (img):
	return [x for x in img.keys() if 'UserComment' in img[x] and img[x]['UserComment'] != 0]

def appendframetofile(frame, tofile, format="jpeg"):
    out = StringIO()
    frame.save(out, format=format)
    tofile.write(out.getvalue())

# oo = orderings[:]
count = 0
walkordering = None
with open (args.output, "w") as f:

	# pick random starting image
	img = data[choice(names)]
	frame = Image.new("RGBA", (args.width, args.height), color=args.background)
	for i in range(args.hold):
		appendframetofile(frame, f)

	# LOOP here
	while count < args.limit:
		# honor walkordering if set (loop)
		imgorderings = nonzeroorderings(img)
		if walkordering:
			try:
				imgorderings.remove(walkordering)
			except ValueError:
				pass
			# HOLD
			for i in range(args.hold):
				appendframetofile(frame, f)

		# construct image from random layers
		shuffle(imgorderings)
		for i, ro in enumerate(imgorderings):
			# ipath = fixname(rimg[ro]['SourceFile'])
			ipath = img[ro]['SourceFile']
			print ipath
			ipath = usepath(ipath)
			draw_layer(frame, ipath)
			# os.rename(ipath, "original{0:04d}.png".format(i))
			# img.save("output{0:04d}.png".format(i))
			appendframetofile(frame, f)
		for i in range(args.hold):
			appendframetofile(frame, f)

		# select random layer, solo it
		walkordering = choice(imgorderings)
		frame = Image.new("RGBA", (args.width, args.height), color=args.background)
		ipath = usepath(img[walkordering]['SourceFile'])
		draw_layer(frame, ipath)
		for i in range(args.hold):
			appendframetofile(frame, f)

		# WALK across
		walkitems = ordered_images[walkordering]
		curindex = walkitems.index(img)
		print "img in {2} is item {0} of {1}".format(curindex, len(walkitems), walkordering)
		direction = choice((1, -1))
		number = randint(1, 10)
		walkitems = walkitems[curindex+direction:curindex+direction+(direction*number):direction]

		print "direction", direction, "number", number
		for img in walkitems:
			frame = Image.new("RGBA", (args.width, args.height), color=args.background)
			ipath = img[walkordering]['SourceFile']
			print ipath
			ipath = usepath(ipath)
			draw_layer(frame, ipath)
			appendframetofile(frame, f)

		count += 1

