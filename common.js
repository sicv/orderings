var LAYER_ORDER = ["preview", "channel_red", "channel_green", "channel_blue", "gradient", "contours", "sift", "texture", "lexicality"];
function layer_sort_val (name) {
	var ret = LAYER_ORDER.indexOf(name);
	return ret == -1 ? 10000 : ret;
}
function layer_sort (a, b) {
		a = layer_sort_val(a);
		b = layer_sort_val(b);
		if (a < b) {
			return -1
		}
		if (b < a) {
			return 1;
		}
		return 1;
}