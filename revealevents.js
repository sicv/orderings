/* requires RTree.js */
function revealevents (elt, opts) {
	var that = {elt: elt},
		rtree = RTree(),
		actives = {},
		lastr = {},
		INTERVAL_TIME = 250,
		debug = false,
		debug_div = null;

	if (debug) {
		debug_div = document.createElement("div");
		elt.appendChild(debug_div);
		debug_div.style.position = "absolute";
		debug_div.style.border = "5px solid red";
	}

	function rect_intersect (a, b) {
		var l = Math.max(a.left, b.left),
			r = Math.min(a.right, b.right),
			t = Math.max(a.top, b.top),
			b = Math.min(a.bottom, b.bottom);
		if (r >= l && b >= t) {
			return {left: l, top: t, right: r, bottom: b};
		} else {
			return {left: 0, top: 0, right: 0, bottom: 0};
		};
	}

	function visible_rect (e) {
		// WANT: rectangle in elements own coordinate system that is in the visible viewport
		// return intersection of viewport with elements BCR
		var bcr = e.getBoundingClientRect(),
			viewport = {left: 0, top: 0, right: window.innerWidth, bottom: window.innerHeight };
			inter = rect_intersect(bcr, viewport);
		// translate intersection back to elt coordinates
		inter.left -= bcr.left; inter.right -= bcr.left;
		inter.top -= bcr.top;  inter.bottom -= bcr.top;
		return inter;
	}

	function add (querySelector) {
		var pr = elt.getBoundingClientRect(),
			elts = elt.querySelectorAll(querySelector),
			i=0,
			len = elts.length,
			celt, cbcr;
		for (i=0; i<len; i++) {
			celt= elts[i];
			cr = celt.getBoundingClientRect();
			var r = {
				x: cr.left - pr.left,
				y: cr.top - pr.top,
				w: cr.right - cr.left,
				h: cr.bottom - cr.top
			};
			// if (i == 0) { console.log("revealevents.add", r); }
			rtree.insert(r, celt);
		}
	}
	that.add = add;

	function remove (elt) {
		// ok this is weird in that it would fail if the element has moved
		// should handle this internally to either remember where the elt
		// was or at least then use the total bounds to search for it
		var pr = that.elt.getBoundingClientRect(),
			cr = elt.getBoundingClientRect(),
			r = {
				x: cr.left - pr.left,
				y: cr.top - pr.top,
				w: cr.right - cr.left,
				h: cr.bottom - cr.top
			};
			// r = {x: cr.x - pr.x, y: cr.y - pr.y, w: cr.width, h: cr.height};
		rtree.remove(r, elt);
	}
	that.remove = remove;

	// pseudo
	// A = {}
	// B = []
	// enter = []
	// for b in B:
	// 	if b not in A:
	// 		enter.append(b)
	//     else:
	//     	A.remove(b)
	// exit = A

	function dr (r) {
		return "["+r.left+","+r.top+"-"+r.right+","+r.bottom+"]";
	}

	function interval () {
		var visiblerect = visible_rect(that.elt);

		// console.log(bcr);
		// show the bcr (debug)
		if (lastr.left == visiblerect.left &&
			lastr.top == visiblerect.top &&
			lastr.right == visiblerect.right &&
			lastr.bottom == visiblerect.bottom) {
			return;
		}
		// console.log("change", dr(visiblerect));
		lastr = visiblerect;
		
		if (debug && debug_div) {
			debug_div.style.left = visiblerect.left+"px";
			debug_div.style.top = visiblerect.top+"px";
			debug_div.style.width = ((visiblerect.right - visiblerect.left)-18)+"px";
			debug_div.style.height = ((visiblerect.bottom - visiblerect.top)-18)+"px";
		}

		var overlaps = rtree.search({x: visiblerect.left, y: visiblerect.top, w: (visiblerect.right - visiblerect.left), h: (visiblerect.bottom - visiblerect.top)});
		var enter = [], exit = [];
		var next_actives = {};
		// console.log("overlaps", overlaps[0], "...", overlaps[overlaps.length-1]);
		//console.log("overlaps", overlaps.length);
		// use actives to deactivate...
		for (var i=0, l=overlaps.length; i<l; i++) {
			var elt = overlaps[i],
				elt_id = opts.elt_id.call(elt);
			next_actives[elt_id] = true;
			if (actives[elt_id] !== undefined) {
				delete actives[elt_id]
			} else {
				enter.push(elt);
			}
		}
		for (var elt_id in actives) { exit.push(elt_id); }
		actives = next_actives;
		if (debug) { console.log("enter", enter.length, "exit", exit.length); }
		// console.log(actives);
		if (opts.reveal) {
			for (var i=0, len = enter.length; i<len; i++) {
				opts.reveal.call(enter[i]);
			}
		}
		if (opts.conceal) {
			for (var i=0, len = exit.length; i<len; i++) {
				opts.conceal.call(exit[i]);
			}			
		}

	}
	window.setInterval(interval, INTERVAL_TIME);
	return that;
}
