import csv, sys


csvpath = sys.argv[1]
count = 0
firstrow = None

print """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" href="styles.css" />
</head>
<body>"""

print """<div class="row">"""

for row in csv.reader(open(csvpath)):
    if not firstrow:
        firstrow = row
        print """<h2>{0}</h2>""".format(row[1])
    else:
        print """<div class="col">
    <div class="original">
        <img src="../material.80/{0}" title="{1}" />
    </div>
    <div class="metadata">
        <span class="value">{1}</span>
    </div>
</div>
""".format(row[0], row[1])


print """</div>"""
print """</body>
</html>"""

