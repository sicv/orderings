import csv, sys, os

csvpath = sys.argv[1]
count = 0
firstrow = None

for row in csv.reader(open(csvpath)):
    if not firstrow:
        firstrow = row
    else:
        imgpath = "../material.320/{0}".format(row[0])
        if os.path.exists(imgpath):
            print """    <td><img src="../material.320/{0}" title="{1}" /></td>""".format(row[0], row[1])

