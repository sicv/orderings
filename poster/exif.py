#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
usage

cd into the folder of images...
python /path/to/exif2csv.py *.jpg > dump.csv

"""

import pyexiv2, sys, csv, os


out = csv.writer(sys.stdout)

data = []
# sort by datetime
# question of orders -- does the scanning order match the tagging order?
# how to 

exif_tags = """
Exif.Photo.DateTimeOriginal
Exif.Image.Make
Exif.Image.Model
Exif.Photo.ExposureTime
Exif.Photo.ExposureTime float
Exif.Photo.FNumber
Exif.Photo.FNumber float
Exif.Photo.ShutterSpeedValue
Exif.Photo.ShutterSpeedValue float
Exif.Photo.ApertureValue
Exif.Photo.ApertureValue float
Exif.Photo.FocalLength
Exif.Photo.FocalLength float
""".strip().splitlines()

headers = ['filename']
headers.extend(exif_tags)
out.writerow(headers)

for p in sys.argv[1:]:
    meta = pyexiv2.ImageMetadata(p)
    meta.read()
#    for key in meta.keys():
#        print key
#    sys.exit(0)
    _, fn = os.path.split(p)
    row = [fn]
    for t in exif_tags:
        if " " in t:
            t, typ = t.split(" ")
        else:
            typ = None

        try:
            v = meta[t]
            if not typ:
                v = v.raw_value
            elif typ == "float":
                v = float(v.value)

        except KeyError:
            # sys.stderr.write("Warning: {0} missing in {1}".format(t, p))
            v = ""
        row.append(v)
    out.writerow(row)



