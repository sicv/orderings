import csv

rows = []
for row in csv.DictReader(open("orderings.csv")):
    rows.append(row)

keys = rows[0].keys()
keys.sort()

print """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" href="styles.css" />
</head>
<body>"""

for key in keys:
    print """<div class="row">
<h2>{0}</h2>""".format(key)
    for row in rows:
        print """<img src="../material/joint.320/{0}" />""".format(row.get(key))
    print """</div>"""
    print


print """</body>
</html>"""

