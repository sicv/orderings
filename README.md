Orderings
===========
Requirements: exiftool

    apt-get install libimage-exiftool-perl libtesseract3 python-opencv 
    libopencv-contrib2.3 qtcreator tesseract-ocr-deu tesseract-ocr-eng 
    leptonica vlfeat.org (vlfeat executable) pylab
