from __future__ import print_function
from PCV.localdescriptors.sift import process_image, read_features_from_file, plot_features
from pylab import *
from PIL import Image
from argparse import ArgumentParser
import os


p = ArgumentParser()
p.add_argument("input", help="path to input image")
p.add_argument("--output", default=None, help="output filename for features (default: image(-ext).sift)")
p.add_argument("--force", default=False, action="store_true")
p.add_argument("--draw", default=None, help="file to draw image (svg)")
p.add_argument("--count", default=False, action="store_true", help="output number of features")
args = p.parse_args()

impath = args.input
if args.output:
	siftpath = args.output
else:
	siftpath = os.path.splitext(impath)[0] + ".sift"

if args.force or (not os.path.exists(siftpath)):
	process_image(impath,siftpath)

if args.draw:
	pil_im = Image.open(impath).convert('L')
	im = array(pil_im)
	l, d = read_features_from_file(siftpath)
	with open(args.draw, "w") as f:
		print ("""<?xml version="1.0"?>
<svg viewBox="0 0 {0} {1}" version="1.1"
  xmlns="http://www.w3.org/2000/svg">
""".format(pil_im.size[0], pil_im.size[1]), file=f)
		for x, y, s, r in l:
			print ("""<circle cx="{0}" cy="{1}" r="{2}" fill="none" stroke="magenta" stroke-width="2" />""".format(x, y, r), file=f)
		print ("""</svg>""", file=f)
	# figure()
	# gray()
	# plot_features(im,l, circle=True)
	# #show()
	# savefig(args.draw)
	if args.count:
		print (len(l))
