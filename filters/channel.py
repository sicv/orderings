from argparse import ArgumentParser
from PIL import Image
import numpy
from numpy import zeros, array

p = ArgumentParser("Isolates one channel by subtracting the others and thresholding")
p.add_argument("--channel", default="red", help="channel to extract (red, green, blue), default: red")
p.add_argument("--output", default="output.png", help="output image path")
p.add_argument("--threshold", type=int, default=16, help="threshold, default: 16")
p.add_argument("input")
args = p.parse_args()

# added convert to rgb to ensure rgb (though this means grayscale images are treated as 3 channel... kind of weird)
im = Image.open(args.input).convert('RGB')
print im, im.size
a = array(im)
#a = array(im.getdata())
#a.shape = (im.size[1], im.size[0], a.shape[-1])

w, h, c = a.shape

out = zeros(a.shape, "uint8")

for x in range(w):
	for y in range(h):
		p = a[x][y]
		# straight channel extraction
		# out[x][y][0] = p[0]

		# subtraction
		# print p,  p[0], max(0, ((p[1]+p[2])/2))
		if args.channel == "red":
			val = max(0, int(p[0]) - ((int(p[1]) + int(p[2])) / 2) )
		elif args.channel == "green":
			val = max(0, int(p[1]) - ((int(p[0]) + int(p[2])) / 2) )
		elif args.channel == "blue":
			val = max(0, int(p[2]) - ((int(p[0]) + int(p[1])) / 2) )

		# print val
		if val > args.threshold:
			val = 255
		else:
			val = 0

		if args.channel == "red":
			out[x][y][0] = val
		elif args.channel == "green":
			out[x][y][1] = val
		elif args.channel == "blue":
			out[x][y][2] = val

# a[:,:,1] *= 0
# a[:,:,2] *= 0
Image.fromarray(out).save(args.output)
# a.save("output.png")
