from PIL import Image
from argparse import ArgumentParser


p = ArgumentParser("Take an image, return overall brightness as a single number (max 255*3)")
p.add_argument("input", help="image path")
p.add_argument("--format", default="number", help="format: (number*, json) *default")
args = p.parse_args()
im = Image.open(args.input)
im.thumbnail((1, 1), Image.ANTIALIAS)
out = sum(im.getpixel((0, 0)))

if args.format == "json":
	import json
	d = {}
	d['input'] = args.input
	d['value'] = out
	print json.dumps(d)
else:
	print out