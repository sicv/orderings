import json, os
from PIL import Image 
from argparse import ArgumentParser
from cStringIO import StringIO
from math import floor, ceil
from sh import inkscape
from random import choice, shuffle, randint

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import A4


"""
variations to try:
1. compact all layers build up to a single jump (to all layer)
2. (optionally) include the original image

"""

p = ArgumentParser("")
p.add_argument("--width", type=int, default=640, help="image scale width, default: 640")
p.add_argument("--height", type=int, default=480, help="image scale height, default: 480")
p.add_argument("--background", default="black", help="background color, default: black")
p.add_argument("--output", default="output.pdf", help="output page filename, default: output.pdf")
p.add_argument("--limit", type=int, default=5, help="total number of cycles to complete, default: 5")
# p.add_argument("--hold", type=int, default=10, help="")
p.add_argument("--rows", type=int, default=3, help="number of rows of images per page")
p.add_argument("--cols", type=int, default=3, help="number of columns of images per page")
p.add_argument("--basepath", default=".", help="where are the images really, default: .")
p.add_argument("--border", type=float, default=1.0, help="border size in cm, default: 1.0")
p.add_argument("--original", action="store_true", default=False, help="include original (after all layers)")
p.add_argument("--incremental", action="store_true", default=False, help="build up layers one by one")
p.add_argument("--skip-layers", action="store_true", default=False, help="don't show the layers")

p.add_argument("--min-walk", type=int, default=1, help="min number of items to walk across, default: 1")
p.add_argument("--max-walk", type=int, default=10, help="max number of items to walk across, default: 10")

args = p.parse_args()


"""
page

-------
-C-C-C-
-------
-C-C-C-
-------
-C-C-C-
-------


"""
# calculate cell width & height based on page size, rows & cols, and border
border = args.border * cm
print ("border", border)
image_width = (A4[0] - (border * (args.cols+1))) / args.cols
image_height = (A4[1] - (border * (args.rows+1))) / args.rows
print ("image size", image_width, image_height)
(col, row) = (0,0)


with open ("index.json") as f:
	data = json.load(f)
	# print json.dumps(data, indent=2	)

names = data.keys()
names.sort()
orderings = data[names[0]].keys()
orderings.sort()

# Created ordered lists per ordering
ordered_images = {}
for o in orderings:
	items = [x for x in data.values() if o in x and 'UserComment' in x[o]]
	items.sort(key=lambda d: d[o]['UserComment'], reverse=True)
	ordered_images[o] = items

	# print "Ordering {0}".format(o)
	# for i in items[:10]:
	# 	print i[o]
	# print "----"
	# print

def fixname (n):
	if n.startswith("sift/"):
		n = n + ".svg"
	elif n.startswith("contours/") or n.startswith("gradient/"):
		n = n[:-4]+".svg"
	return n

def usepath (ipath):
	ipath = fixname(ipath)
	ipath = os.path.join(args.basepath, ipath)
	if ipath.endswith(".svg"):
		inkscape("-w", str(args.width), "--export-png=tmp.png", ipath)
		ipath = "tmp.png"
	return ipath

def draw_layer(base, path):
    W, H = base.size
    im = Image.open(path)
    if im.mode != "RGBA":
    	im = im.convert("RGBA")
    im.thumbnail((W,H))
    w, h = im.size
    ix = (W-w)/2
    iy = (H-h)/2
    # bim = Image.new("RGB", (W,H), color=args.background)
    base.paste(im, (ix, iy), mask=im)

def nonzeroorderings (img):
	return [x for x in img.keys() if 'UserComment' in img[x] and img[x]['UserComment'] != 0]

# def appendframetofile(frame, tofile, format="jpeg"):
#     out = StringIO()
#     frame.save(out, format=format)
#     tofile.write(out.getvalue())

def appendframetofile(frame, tofile, format="jpeg"):
	global row, col
	cw = (image_width + border)
	ch = (image_height + border)
	x = border + (col*cw)
	y = border + (args.rows-row-1)*ch
	tofile.drawImage(ImageReader(frame), x, y, image_width, image_height)
	col += 1
	if (col >= args.cols):
		col = 0
		row += 1
		if (row >= args.rows):
			tofile.showPage()
			row = 0

# oo = orderings[:]
count = 0
walkordering = None
# with open (args.output, "w") as f:

# pick random starting image
img = data[choice(names)]
frame = Image.new("RGBA", (args.width, args.height), color=args.background)
# for i in range(args.hold):
#	appendframetofile(frame, f)



f = canvas.Canvas(args.output)
# c.save()


# LOOP here
while count < args.limit:
	# honor walkordering if set (loop)
	imgorderings = nonzeroorderings(img)
	if walkordering:
		try:
			imgorderings.remove(walkordering)
		except ValueError:
			pass
		# HOLD
		# for i in range(args.hold):
		# 	appendframetofile(frame, f)

	# construct image from random layers
	if not args.skip_layers:
		print "build up a single image layers"
		shuffle(imgorderings)
		for i, ro in enumerate(imgorderings):
			ipath = img[ro]['SourceFile']
			ipath = usepath(ipath)
			print ipath
			draw_layer(frame, ipath)
			if args.incremental:
				appendframetofile(frame, f)

		if not args.incremental:
			appendframetofile(frame, f)

	# for i in range(args.hold):
	# 	appendframetofile(frame, f)
	if args.original:
		ipath = img["preview"]['SourceFile']
		ipath = usepath(ipath)
		print ipath
		frame = Image.new("RGBA", (args.width, args.height), color=args.background)
		draw_layer(frame, ipath)
		# for i in range(args.hold):
		appendframetofile(frame, f)

	# select random layer, solo it
	print "select random layer, solo it"
	walkordering = choice(imgorderings)
	frame = Image.new("RGBA", (args.width, args.height), color=args.background)
	ipath = usepath(img[walkordering]['SourceFile'])
	print ipath
	draw_layer(frame, ipath)
	# for i in range(args.hold):
	appendframetofile(frame, f)

	# WALK across
	print "walk across"
	walkitems = ordered_images[walkordering]
	curindex = walkitems.index(img)
	print "img in {2} is item {0} of {1}".format(curindex, len(walkitems), walkordering)
	direction = choice((1, -1))
	number = randint(args.min_walk, args.max_walk)
	walkitems = walkitems[curindex+direction:curindex+direction+(direction*number):direction]

	print "direction", direction, "number", number
	for img in walkitems:
		frame = Image.new("RGBA", (args.width, args.height), color=args.background)
		ipath = img[walkordering]['SourceFile']
		ipath = usepath(ipath)
		print ipath
		draw_layer(frame, ipath)
		appendframetofile(frame, f)

	count += 1

f.save()
